﻿using Flurl;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Prediction_service.Enumerations;
using Prediction_service.Models.DTO;
using Prediction_service.Models.DTO.Solar;
using Prediction_service.Models.Entity.solar;
using RabbitMQ_Eventbus.Eventbus;
using RabbitMQ_Eventbus.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Logic
{
	public class SolarPrediction
	{

		public SolarPrediction( ) {
		}

		private int GetProductionOfLocation(string location)
		{

			switch (location)
			{
				case "Noord-Holland":
					return 2877909;
				case "Zuid - Holland":
					return 3600000;
				case "Utrecht":
					return 359376;
				default:
					return 17474000; //total population
			}
		}
		private double GetRainFactor(int rainfall)
		{
			double rainFactor;
			
			if(rainfall <= 33)
			{
				rainFactor = (double)WeatherConditions.Rainfall_none / 10;
			}else if(rainfall >33 && rainfall <= 66)
			{
				rainFactor = (double)WeatherConditions.Rainfall_little / 10;
			}
			else 
			{
				rainFactor = (double)WeatherConditions.Rainfall_heavy / 10;
			}

			return rainFactor;
		}
		private double GetCloudCoverFactor(int cloudCover)
		{
			double cloudFactor;

			if (cloudCover <= 33)
			{
				cloudFactor = (double)WeatherConditions.CloudCover_none / 10;
			}
			else if (cloudCover > 33 && cloudCover <= 66)
			{
				cloudFactor = (double)WeatherConditions.CloudCover_little / 10;
			}
			else
			{
				cloudFactor = (double)WeatherConditions.CloudCover_heavy / 10;
			}

			return cloudFactor;
		}
		public async Task<SolarMessage> CalculateOutput(string timesteps, string location, string start, string end)
		{
			int solarPanelCount = GetProductionOfLocation(location);
			const double nominalPower = 300; // wattpiek
			const double solarPanelArea = 1.65 * 1.00; // m²
			const double efficiency = 0.192;

			double totalSolarPanelArea = solarPanelArea * solarPanelCount;
			List<SolarData> results = new List<SolarData>();
			string city = "4.937386794198567"+ "," + "52.40016844635332"; //amsterdam

			var url = $"https://api.tomorrow.io/v4/timelines?apikey=Fl4XGx6KhGDvgDnQpSBzoioaZT9rlr5X&location={city}&fields=solarGHI&units=metric&timesteps={timesteps}&timezone=Europe/Amsterdam&startTime={start}&endTime={end}";

			var response = await url.GetJsonAsync<SolarResponse>();

			var solarMessage = new SolarMessage
			{
				SolarIntervals = new List<SolarData>(),
				StartTime = response.Data.Timelines[0].StartTime,
				EndTime = response.Data.Timelines[0].EndTime,
			};

			foreach(Interval data in response.Data.Timelines[0].Intervals)
			{
				double GHIFactor = data.Values.SolarGHI / 1000;
				double currentTotalSolarPower = totalSolarPanelArea * GHIFactor * efficiency;

				results.Add(new SolarData{
					GHI = (int)currentTotalSolarPower,
					Date = data.StartTime
				});
			}

			var message = new SolarMessage
			{
				SolarIntervals = results,
				StartTime = solarMessage.StartTime,
				EndTime = solarMessage.EndTime
			};

			//var json = JsonConvert.SerializeObject(message);

			return message;
			//_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.prediction.solar"), json));
		}
	}
}
