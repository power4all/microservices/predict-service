﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prediction_service.Models.DTO.Wind;
using Prediction_service.Models.Entity.wind;

namespace Prediction_service.Logic
{
	public class WindPrediction
	{
		private double radius = Math.Pow(58, 2);
		private double efficiencyFactor = 0.4;
		
		public async Task<WindMessage> CalculateOutput(string timesteps, string location, int windspeed, int temprature, string start, string end)
		{
			List<WindData> results = new List<WindData>();
			double airDensity = getAirDensity(temprature);
			double totalAmountOfWindMills = this.GetProductionOfLocation(location);
			string city = "4.937386794198567" + "," + "52.40016844635332"; //amsterdam

			var url = $"https://api.tomorrow.io/v4/timelines?apikey=Fl4XGx6KhGDvgDnQpSBzoioaZT9rlr5X&location={city}&fields=windSpeed&units=metric&timesteps={timesteps}&timezone=Europe/Amsterdam&startTime={start}&endTime={end}";
			var response = await url.GetJsonAsync<WindRepsonse>();
			var windMessage = new WindMessage
			{
				WindSpeedIntervals = new List<WindData>(),
				StartTime = response.Data.Timelines[0].StartTime,
				EndTime = response.Data.Timelines[0].EndTime,
			};

			foreach (Interval data in response.Data.Timelines[0].Intervals)
			{
				var speedFactor = (windspeed / data.Values.windSpeed * 10) + data.Values.windSpeed;
				double speed = Math.Pow(speedFactor, 3);
				var currentTotalWindPower = (int)((Math.PI / 2) / 1000 * radius * speed * airDensity * efficiencyFactor * totalAmountOfWindMills);

				results.Add(new WindData
				{
					WindSpeed = (int)currentTotalWindPower,
					Date = data.StartTime
				});
			}

			windMessage.WindSpeedIntervals = results;
			return windMessage;

		}

		private int GetProductionOfLocation(string location)
		{

			switch (location)
			{
				case "Noord-Holland":
					return 72;  // 16,5 %
				case "Zuid - Holland":
					return 90;  //20,6 %
				case "Utrecht":
					return 9;   //2,1 %
				default:
					return 437; //total population
			}
		}

		private double getAirDensity(int temprature)
		{
			double airDensity = 0.0;
			if(temprature > 0 && temprature <= 5)
			{
				airDensity = 1.2922;
			}else if(temprature > 5 && temprature <= 10)
			{
				airDensity = 1.2690;
			}
			else if (temprature > 10 && temprature <= 15)
			{
				airDensity = 1.2466;
			}
			else if (temprature > 15 && temprature <= 20)
			{
				airDensity = 1.2250;
			}
			else if (temprature > 20 && temprature <= 25)
			{
				airDensity = 1.19;
			}
			else if(temprature > 25)
			{
				airDensity = 1.14;
			}
			else
			{
				airDensity = 1.2;
			}

			return airDensity;
		}
	}
}
