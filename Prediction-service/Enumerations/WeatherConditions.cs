﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Enumerations
{
	public enum WeatherConditions
	{
		Rainfall_none = 10,				// Geen regen betekend consistent
		Rainfall_little = 5,			// Beetje regen is nadeling i.v.m. de bewolking, maar voordelig qua koeling
		Rainfall_heavy = 3,				// De donkere wolken absoberen meer licht
		CloudCover_none = 10,			// Geen bewolking betekend consistent
		CloudCover_little = 12,			// Beetje bewolking betekend lichte verbetering
		CloudCover_heavy = 15		// Stapelbewolking zorgt ervoor dat het overdag koeler is waardoor de panelen effiecienter zijn
	}
}
