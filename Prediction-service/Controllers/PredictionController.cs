﻿using Microsoft.AspNetCore.Mvc;
using Prediction_service.Logic;
using Prediction_service.Models.Entity.solar;
using Prediction_service.Models.Entity.wind;
using RabbitMQ_Eventbus.Eventbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Controllers
{
	[Route("api/prediction")]
	[ApiController]
	public class PredictionController : Controller
	{
		private SolarPrediction _solar;
		private WindPrediction _wind;

		public PredictionController()
		{
			this._solar = new SolarPrediction();
			this._wind = new WindPrediction();
		}
		[HttpGet("test")]
		public List<string> GetTest()
		{
			List<string> t = new List<string>();
			t.Add("hiii");
			return t;
		}

		[HttpGet("solar")]
		public SolarMessage GetSolarPrediction([FromQuery]string timesteps, [FromQuery]string location, [FromQuery]string start, [FromQuery]string end, [FromQuery]int temprature)
		{	
			 var task = this._solar.CalculateOutput(timesteps, location, start, end);
			return task.Result;
		}

		[HttpGet("wind")]
		public WindMessage GetWindPrediction([FromQuery] string timesteps, [FromQuery] string location, [FromQuery]int windspeed, [FromQuery] int temprature, [FromQuery] string start, [FromQuery] string end)
		{
			var task = this._wind.CalculateOutput(timesteps, location, windspeed, temprature,start,end);
			return task.Result;
		}
	}
}
