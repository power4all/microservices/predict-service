﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Prediction_service.Models.DTO;
using RabbitMQ.Client.Events;
using RabbitMQ_Eventbus.FunctionProvider;
using RabbitMQ_Eventbus.Message;
using System;
using System.Collections.Generic;
using Prediction_service.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prediction_service.Models.Entity.solar;
using Microsoft.EntityFrameworkCore;

namespace Prediction_service.RabbitMQEventbus
{
	public class FunctionProvider : RabbitMQFunctionProviderBase
	{
		private readonly ILogger<FunctionProvider> _logger;
		private readonly IServiceScopeFactory _scopeFactory;

		public FunctionProvider(ILogger<FunctionProvider> logger, IServiceScopeFactory scopeFactory)
		{
			_logger = logger;
			_scopeFactory = scopeFactory;
		}

		protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>> InializeFunctionsWithKeys()
		{
			var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
			dictionary.Add("HandleProductionData", HandleProductionData);

			return dictionary;
		}

		private RabbitMQMessage HandleProductionData(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var message = Encoding.UTF8.GetString(body);

			try
			{
				var msg = Newtonsoft.Json.JsonConvert.DeserializeObject<ProducerMessage>(message);
				using (var scope = _scopeFactory.CreateScope() )
				{
					var dbcontext = scope.ServiceProvider.GetRequiredService<ApplicationDBContext>();
					dbcontext.ProducerMessages.Add(msg);
					dbcontext.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex.StackTrace);
			}

			return null;
		}

		private RabbitMQMessage HandleSolarData(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var message = Encoding.UTF8.GetString(body);

			try
			{
				var solarMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<SolarMessage>(message);

				using (var scope = _scopeFactory.CreateScope())
				{
					var dbcontext = scope.ServiceProvider.GetRequiredService<ApplicationDBContext>();

					// delete previous 
					dbcontext.Database.ExecuteSqlRaw("DELETE FROM SolarData;");

					foreach (var interval in solarMessage.SolarIntervals)
					{
						dbcontext.SolarData.Add(interval);
					}
					dbcontext.SaveChanges();
				}
			}
			catch (Exception e)
			{
				_logger.LogWarning(e.StackTrace);
			}

			return null;
		}
	}
}
