﻿using Microsoft.EntityFrameworkCore;
using Prediction_service.Models.DTO;
using Prediction_service.Models.Entity.solar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Data
{
	public class ApplicationDBContext : DbContext
	{
		public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options) { }
		public DbSet<ProducerMessage> ProducerMessages { get; set; }
		public DbSet<ProducerTypes> ProducerTypes { get; set; }
		public DbSet<SolarData> SolarData { get; set; }
	}
}
