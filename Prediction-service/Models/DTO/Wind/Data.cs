﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.DTO.Wind
{
	public class Data
	{
		public List<Timeline> Timelines { get; init; }
	}
}
