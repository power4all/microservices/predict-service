﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.DTO.Wind
{
	public class Values
	{
		public double windSpeed { get; init; }
		public double temprature { get; init; }
	}
}
