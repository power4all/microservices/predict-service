﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.DTO
{
	public class ProductionSubtotalEnergyMessage
	{
		public int TotalOutput { get; set; }
		public int ProducerCount { get; set; }
		public string ProducerType { get; set; }
	}
}
