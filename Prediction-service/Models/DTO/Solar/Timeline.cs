﻿using Prediction_service.Models.DTO.Solar;
using System;
using System.Collections.Generic;

public class Timeline
{
    public string Timestep { get; init; }
    public DateTime StartTime { get; init; }
    public DateTime EndTime { get; init; }
    public List<Interval> Intervals { get; init; }
}