﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.DTO.Solar
{
    public class Interval
    {
        public DateTime StartTime { get; init; }
        public Values Values { get; init; }
    }
}
