﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Prediction_service.Models.Entity.solar
{
    public class SolarData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; init; }
        public double GHI { get; init; }
        public DateTime Date { get; init; }
    }
}
