﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.Entity.solar
{
    public class SolarMessage
    {
        public List<SolarData> SolarIntervals { get; init; }
        public DateTime StartTime { get; init; }
        public DateTime EndTime { get; init; }
    }
}
