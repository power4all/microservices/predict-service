﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.Entity.wind
{
	public class WindData
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; init; }
		public double WindSpeed { get; init; }
		public DateTime Date { get; init; }
	}
}
