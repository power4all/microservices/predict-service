﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prediction_service.Models.Entity.wind
{
	public class WindMessage
	{
		public List<WindData> WindSpeedIntervals { get; set; }
		public DateTime StartTime { get; init; }
		public DateTime EndTime { get; init; }
	}
}
